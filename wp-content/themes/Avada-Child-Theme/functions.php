<?php


/*** WP max photo upload size - Ultimate Member cover photo/profile photo upload fix

***/
@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );

/*** End WP max photo upload size - Ultimate Member cover photo/profile photo upload fix

***/


/*** Add Tracking Code to the Thank You Page - http://danielsantoro.com/add-facebook-tracking-pixel-woocommerce-checkout/

***/

function ds_checkout_analytics( $order_id ) {

	$order = new WC_Order( $order_id );

	$currency = $order->get_order_currency();

	$total = $order->get_total();

	$date = $order->order_date;

	?>

	<!-- Paste Tracking Code Under Here -->

<!-- Facebook Pixel Code -->

<script>

  !function(f,b,e,v,n,t,s)

  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?

  n.callMethod.apply(n,arguments):n.queue.push(arguments)};

  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';

  n.queue=[];t=b.createElement(e);t.async=!0;

  t.src=v;s=b.getElementsByTagName(e)[0];

  s.parentNode.insertBefore(t,s)}(window, document,'script',

  'https://connect.facebook.net/en_US/fbevents.js');

  fbq('init', '496734663763400');

  fbq('track', 'PageView');

  fbq('track', 'AddToCart');

  fbq('track', 'Purchase', {value: '0.00', currency:'USD'});



</script>

<noscript><img height="1" width="1" style="display:none"

  src="https://www.facebook.com/tr?id=496734663763400&ev=PageView&noscript=1"

/></noscript>

<!-- End Facebook Pixel Code -->





	<!-- End Tracking Code -->

	<?php	

}

add_action( 'woocommerce_thankyou', 'ds_checkout_analytics' );





add_filter('register','no_register_link');

function no_register_link($url){

    return '';

}



function theme_enqueue_styles() {

    wp_enqueue_style( 'avada-parent-stylesheet', get_template_directory_uri() . '/style.css' );

}

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );



function avada_lang_setup() {

	$lang = get_stylesheet_directory() . '/languages';

	load_child_theme_textdomain( 'Avada', $lang );

}

add_action( 'after_setup_theme', 'avada_lang_setup' );



add_filter('jpeg_quality', function($arg){return 90;});

add_filter('show_admin_bar', '__return_false', 99);



// Display variations dropdowns on shop page for variable products

 add_filter( 'woocommerce_loop_add_to_cart_link', 'woo_display_variation_dropdown_on_shop_page' );

 

 function woo_display_variation_dropdown_on_shop_page() {

	 

 	global $product;

	if( $product->is_type( 'variable' )) {

	

	$attribute_keys = array_keys( $product->get_variation_attributes() );

	?>

	

	<form class="variations_form cart" method="post" enctype='multipart/form-data' data-product_id="<?php echo absint( $product->id ); ?>" data-product_variations="<?php echo htmlspecialchars( json_encode( $product->get_available_variations() ) ) ?>">

		<?php do_action( 'woocommerce_before_variations_form' ); ?>

	

		<?php if ( empty( $product->get_available_variations() ) && false !== $product->get_available_variations() ) : ?>

			<p class="stock out-of-stock"><?php _e( 'This product is currently out of stock and unavailable.', 'woocommerce' ); ?></p>

		<?php else : ?>

			<table class="variations" cellspacing="0">

				<tbody>

				<?php foreach ( $product->get_variation_attributes() as $attribute_name => $options ) : ?>

						<tr>

							<td class="label"><label for="<?php echo sanitize_title( $attribute_name ); ?>"><?php echo wc_attribute_label( $attribute_name ); ?></label></td>

							<td class="value">

								<?php

									$selected = isset( $_REQUEST[ 'attribute_' . sanitize_title( $attribute_name ) ] ) ? wc_clean( urldecode( $_REQUEST[ 'attribute_' . sanitize_title( $attribute_name ) ] ) ) : $product->get_variation_default_attribute( $attribute_name );

									wc_dropdown_variation_attribute_options( array( 'options' => $options, 'attribute' => $attribute_name, 'product' => $product, 'selected' => $selected ) );

									echo end( $attribute_keys ) === $attribute_name ? apply_filters( 'woocommerce_reset_variations_link', '<a class="reset_variations" href="#">' . __( 'Clear', 'woocommerce' ) . '</a>' ) : '';

								?>

							</td>

						</tr>

					<?php endforeach;?>

				</tbody>

			</table>

	

			<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

	

			<div class="single_variation_wrap">

				<?php

					/**

					 * woocommerce_before_single_variation Hook.

					 */

					do_action( 'woocommerce_before_single_variation' );

	

					/**

					 * woocommerce_single_variation hook. Used to output the cart button and placeholder for variation data.

					 * @since 2.4.0

					 * @hooked woocommerce_single_variation - 10 Empty div for variation data.

					 * @hooked woocommerce_single_variation_add_to_cart_button - 20 Qty and cart button.

					 */

					do_action( 'woocommerce_single_variation' );

	

					/**

					 * woocommerce_after_single_variation Hook.

					 */

					do_action( 'woocommerce_after_single_variation' );

				?>

			</div>

	

			<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>

		<?php endif; ?>

	

		<?php do_action( 'woocommerce_after_variations_form' ); ?>

	</form>

		

	<?php } else {

		

	echo sprintf( '<a rel="nofollow" href="%s" data-quantity="%s" data-product_id="%s" data-product_sku="%s" class="%s">%s</a>',

			esc_url( $product->add_to_cart_url() ),

			esc_attr( isset( $quantity ) ? $quantity : 1 ),

			esc_attr( $product->id ),

			esc_attr( $product->get_sku() ),

			esc_attr( isset( $class ) ? $class : 'button' ),

			esc_html( $product->add_to_cart_text() )

		);

	

	}

	 

}





?>