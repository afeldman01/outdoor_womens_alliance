<?php
/**
 * Template Name: Benefits Card 
 *
 * @package Avada
 * @subpackage Templates
 */

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
?>
<?php get_header(); ?>
<style>
@media only screen 
and (min-width : 200px)     
and (max-width : 767px)
{
.membercard{border:4px black dashed; padding:5px 20px; margin:5px 0; border-radius:25px; width:98%; background:url("http://www.outdoorwomensalliance.com/wp-content/uploads/2017/07/OWA-logo-mountain-final-badge3-cropped-faded.jpg"); font-size:100%;}
.dates{font-size:70%;}      
}
    
@media only screen 
and (min-width : 768px)     
{
.membercard{border:4px black dashed; padding:5px 20px; margin:5px; border-radius:25px; max-width:60%; background:url("http://www.outdoorwomensalliance.com/wp-content/uploads/2017/07/OWA-logo-mountain-final-badge3-cropped-faded.jpg"); font-size:140%;}
.dates{font-size:70%;}      
}    
</style>
<div id="content" class="full-width">
	<?php while ( have_posts() ) : the_post(); ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<?php echo wp_kses_post( fusion_render_rich_snippets_for_pages() ); ?>
			<?php avada_featured_images_for_pages(); ?>
			<div class="post-content">
				<?php the_content(); ?>
				<?php fusion_link_pages(); ?>
			</div>
			<?php if ( ! post_password_required( $post->ID ) ) : ?>
				<?php if ( Avada()->settings->get( 'comments_pages' ) ) : ?>
					<?php wp_reset_postdata(); ?>
					<?php comments_template(); ?>
				<?php endif; ?>
			<?php endif; ?>
		</div>    
	<?php endwhile; ?>
</div>
<?php
function subscriber_start_date() {
    // Set start date to initial value
    $start_date = FALSE;
    // Get ALL subscriptions
    $subscriptions = WC_Subscriptions_Manager::get_users_subscriptions( $user_id );
    if ($subscriptions) {
        // Get the first subscription
        $subscription = array_shift($subscriptions);
        // Get the start date, if set
        $start_date = (isset($subscription['start_date'])) ? $subscription['start_date'] : FALSE;
    }
    
      return $start_date;
}
if(is_user_logged_in()) {

    //We only need these products to check
    $products_to_check = array(6664);
    $customer_bought = array();

    //Get all orders made by the current user in the last 365 days
    $customer_orders = get_posts( apply_filters( 'woocommerce_my_account_my_orders_query', array(
        'numberposts' => $order_count,
        'meta_key'    => '_customer_user',
        'meta_value'  => get_current_user_id(),
        'post_type'   => wc_get_order_types( 'view-orders' ),
        'post_status' => array('wc-completed', 'wc-processing'),
        'date_query' => array(
            array(
                'column' => 'post_date_gmt',
                'after' => '1 year ago',
            )
        )
    )));
	
    //Loop trough the orders
    foreach ( $customer_orders as $customer_order ) {
		$order = wc_get_order( $customer_order );        
        $items = $order->get_items();

        //Loop trough the order items and see if it is a product we need to cehck
        foreach($items as $item) {          
            if(in_array($item['product_id'], $products_to_check)) {
                $customer_bought[] = $item['product_id'];
            }
        }

    }
	
if(in_array(6664, $customer_bought)) {
 echo'<div style="clear:both"></div>
 <hr />
<div class="membercard">
<img src="http://www.outdoorwomensalliance.com/wp-content/uploads/2016/08/Outdoor-Womens-Alliance-Logo.png">' ;   

um_fetch_user( get_current_user_id() );
$first_name = um_user('first_name');
$last_name = um_user('last_name');
 $display_name = um_user('display_name');
 echo"<p><strong>OWA Member:</strong> $first_name $last_name</p>" ;// returns the display name of logged-in user
 
$roleName = $ultimatemember->user->get_role();
date_default_timezone_get();
$start_date = subscriber_start_date();

$time = strtotime($start_date.' UTC');
$start_date = date("m-d-Y", $time);
$start_date_calc = date("Y-m-d", $time);

$end_date =  date("m-d-Y", strtotime("+1 years", strtotime($start_date_calc)));
echo "<p><strong>Member Type:</strong> $roleName</p>";
    
 function __get( $key ) {
    // Get values or default if not set
    if ( 'completed_date' == $key ) {
        $value = ( $value = get_post_meta( $this->id, '_completed_date', true ) ) ? $value : $this->modified_date;
    } elseif ( 'user_id' == $key ) {
        $value = ( $value = get_post_meta( $this->id, '_customer_user', true ) ) ? absint( $value ) : '';
    } else {
        $value = get_post_meta( $this->id, '_' . $key, true );
    }

    return $value;
}
$order = new WC_Order( $order_id );
$user_id = $order->user_id;             

echo "<span class='dates'><strong>Member Since:</strong> $start_date<br><strong>Card Expires On:</strong> $end_date</span><br><div style='clear:both;'></div></div>";
}	
	
} else {
    echo 'You need to sign in first!';
}

?>
<?php get_footer();

/* Omit closing PHP tag to avoid "Headers already sent" issues. */
