<?php
namespace um_ext\um_mailchimp\core;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) exit;

class Mailchimp_Func {

	private $mailchimp;

	function __construct() {

		$this->user_id = get_current_user_id();

		$this->schedules();

		$this->lists_subscribed = array();

	}

	/***
	***	@Schedules
	***/
	function schedules() {

		add_action( 'um_daily_scheduled_events', array( $this, 'mailchimp_subscribe' ) );

		add_action( 'um_daily_scheduled_events', array( $this, 'mailchimp_unsubscribe' ) );

		add_action( 'um_daily_scheduled_events', array( $this, 'mailchimp_update' ) );

	}

	function filter_connected_lists( $lists ) {
		$um_list_ids = array_keys( $lists );
		foreach ( $um_list_ids as $_list_value ) {

			$args = array(
				'post_type'	=> 'um_mailchimp',
				'meta_query' => array(
					array(
						'key'     => '_um_list',
						'value'   => $_list_value,
						'compare' => '=',
					),
				)
			);

			$um_list_query = new \WP_Query( $args );
			if( !$um_list_query->post_count ){
				 unset( $lists[ $_list_value ] );
		   }
		}
		return $lists;
	}

	function prepare_data( $merge_vars ) {
		$merge_vars = empty(  $merge_vars ) ? array() : $merge_vars;
		foreach( $merge_vars as $key => $val ) {
			if( ! empty( $val ) ) {
				if ( is_array( $val ) ) {
					$merge_vars[ $key ] = implode(', ', $val );
				}
			}else{
				unset( $merge_vars[ $key ] );
			}
		}
		if( isset( $merge_vars['email_address'] ) ) {
			unset( $merge_vars['email_address'] );
		}
		return $merge_vars;
	}

	/***
	***	@Update
	***/
	function mailchimp_update( $override = false, $all = true ) {

		$last_send = $this->get_last_update();
		if( !$override && $last_send && $last_send > strtotime( '-1 day' ) )
			return;

		$array = get_option('_mailchimp_new_update');
		if ( !$array || !is_array($array) ) $array = array();

		$array = $this->filter_connected_lists( $array );

		// update user info for specific list
		foreach( $array as $list_id => $data ) {
			if ( empty( $data ) ) continue;

			foreach( $data as $user_id => $merge_vars ) {
				if( !empty( $merge_vars['email_address'] ) ) {
					$email = $merge_vars['email_address'];
				} else {
					um_fetch_user( $user_id );
					$email = um_user('user_email');
				}

				$email_md5 = md5( $email );

				$this->call()->put("lists/{$list_id}/members/{$email_md5}",  array(
					'email_address' => um_user('user_email'),
					'merge_fields'  => $this->prepare_data( $merge_vars ),
					'status'        => apply_filters('um_mailchimp_default_subscription_status', 'subscribed', 'update', $list_id, $email ),
				));

				unset( $array[$list_id][$user_id] );

				// only update one profile at a time
				if( !$all ) break;
			}
		}

		// reset new update sync
		update_option('_mailchimp_new_update', $array);

		// update last update data
		update_option( 'um_mailchimp_last_update', time() );

	}

	/***
	***	@Subscribe
	***/
	function mailchimp_subscribe( $override = false, $all = true ) {
		$last_send = $this->get_last_subscribe();
		if( !$override && $last_send && $last_send > strtotime( '-1 day' ) )
			return;

		$array = get_option('_mailchimp_new_subscribers');
		if ( !$array || !is_array($array) ) $array = array();

		$array = $this->filter_connected_lists( $array );

		// update user info for specific list
		foreach ( $array as $list_id => $data ) {
			if ( empty( $data ) ) {
				continue;
			}

			foreach ( $data as $user_id => $merge_vars ) {
				if( ! empty( $merge_vars['email_address'] ) ) {
					$email = $merge_vars['email_address'];
				} else {
					um_fetch_user( $user_id );
					$email = um_user( 'user_email' );
				}
				$email_md5 = md5( $email );

				$this->call()->put( "lists/{$list_id}/members/{$email_md5}", array(
					'email_address' => $email,
					'merge_fields'  => $this->prepare_data( $merge_vars ),
					'status'        => apply_filters_ref_array( 'um_mailchimp_default_subscription_status', array(
						! empty( $merge_vars['subscr_status'] ) ? $merge_vars['subscr_status'] : 'subscribed',
						'subscribe',
						$list_id,
						$email
					) ),
				));

				unset( $array[ $list_id ][ $user_id ] );

				// only update one profile at a time
				if ( ! $all ) {
					break;
				}
			}
		}
		
		// reset new subscribers sync
		update_option('_mailchimp_new_subscribers', $array);

		// update last subscribe data
		update_option( 'um_mailchimp_last_subscribe', time() );

	}

	/***
	***	@Unsubscribe
	***/
	function mailchimp_unsubscribe( $override = false, $all = true ) {

		$last_send = $this->get_last_unsubscribe();
		if( !$override && $last_send && $last_send > strtotime( '-1 day' ) )
			return;

		$array = get_option('_mailchimp_new_unsubscribers');
		if ( !$array || !is_array( $array ) ) $array = array();

		$array = $this->filter_connected_lists( $array );

		// unsubscribe each user to the mailing list
		foreach( $array as $list_id => $data ) {
			if ( empty( $data ) ) continue;

			foreach( $data as $user_id => $merge_vars ) {
				um_fetch_user( $user_id );
				if( isset( $merge_vars['email_address'] ) ) {
					$email = $merge_vars['email_address'];
				} else {
					$email = um_user('user_email');
				}
				$email_md5 = md5( $email );

				if ( UM()->options()->get('mailchimp_unsubscribe_delete') ) {
					$this->call()->delete( "lists/{$list_id}/members/{$email_md5}" );
				} else {
					$this->call()->patch( "lists/{$list_id}/members/{$email_md5}", array(
						'email_address' => $email,
						'status' => 'unsubscribed',
					) );
				}

				unset( $array[ $list_id ][ $user_id ] );

				// only update one profile at a time
				if( !$all ) break;
			}
		}
		
		// reset new unsubscribers sync
		update_option('_mailchimp_new_unsubscribers', $array);

		// update last unsubscribe data
		update_option( 'um_mailchimp_last_unsubscribe', time() );

	}

	/***
	***	@Last Update
	***/
	function get_last_update() {
		return get_option( 'um_mailchimp_last_update' );
	}

	/***
	***	@Last Subscribe
	***/
	function get_last_subscribe() {
		return get_option( 'um_mailchimp_last_subscribe' );
	}

	/***
	***	@Last Unsubscribe
	***/
	function get_last_unsubscribe() {
		return get_option( 'um_mailchimp_last_unsubscribe' );
	}

	/***
	***	@update user
	***/
	function update( $list_id, $_merge_vars=null ) {
		global $old_email;

		$user_id = $this->user_id;
		um_fetch_user( $user_id );

		if ( !um_user('user_email') ) return;

		$merge_vars = array('FNAME'=> um_user('first_name'), 'LNAME'=> um_user('last_name') );
		$merge_vars = array_filter( $merge_vars );

		if( $mylists = um_user('_mylists') && $list_id ) {
			//prevent updates for not subscribed users
			if( !isset( $mylists[ $list_id ] ) ) return;

			$merge_vars = $this->get_merge_vars_values( $list_id, $user_id, $merge_vars );
			$merge_vars = apply_filters('um_mailchimp_single_merge_fields', $merge_vars, $user_id, $list_id, $_merge_vars );
		} else {
			//prevent updates for not subscribed users
			return;
		}

		$_new_update = get_option('_mailchimp_new_update');
		$_new_update = is_array( $_new_update ) ? $_new_update : array();
		$_new_update[ $list_id ][ $user_id ] = $merge_vars;
		$_new_update[ $list_id ][ $user_id ]['email_address'] = um_user('user_email');

		if( !empty( $old_email ) && $old_email != um_user('user_email') ) {
			$_new_update[ $list_id ][ $user_id ]['subscr_status'] = $this->get_subscription_status_by_list_id( $list_id );
		}

		update_option( '_mailchimp_new_update', $_new_update );

		delete_option( "um_cache_userdata_{$user_id}" );

		if ( UM()->options()->get('mailchimp_real_status') ) {
			$this->mailchimp_update( true );
		}
	}

	/***
	***	@subscribe user
	***/
	function subscribe( $list_id, $_merge_vars=null ) {
		$user_id = $this->user_id;
		um_fetch_user( $user_id );

		if ( ! um_user('user_email') ) return;

		$merge_vars = array('FNAME'=> um_user('first_name'), 'LNAME'=> um_user('last_name') );
		$merge_vars = array_filter( $merge_vars );

		$mylists = um_user('_mylists');

		if( $mylists || $list_id ) {
			$merge_vars = $this->get_merge_vars_values( $list_id, $user_id, $merge_vars );
			$merge_vars = apply_filters('um_mailchimp_single_merge_fields', $merge_vars, $user_id, $list_id, $_merge_vars );
		}

		$_mylists = is_array( $mylists ) ? $mylists : array();
		if ( !isset( $_mylists[ $list_id ] ) ) {
			$_mylists[ $list_id ] = 1;
			update_user_meta( $user_id, '_mylists', $_mylists );
		}
		
		$_new_unsubscribers = get_option('_mailchimp_new_unsubscribers');
		if ( isset( $_new_unsubscribers[ $list_id ][ $user_id ] ) ) {
			unset( $_new_unsubscribers[ $list_id ][ $user_id ] );
		}

		$_new_subscribers = get_option('_mailchimp_new_subscribers');
		$_new_subscribers = is_array( $_new_subscribers ) ? $_new_subscribers : array();
		$_new_subscribers[$list_id][$user_id] = $merge_vars;
		$_new_subscribers[ $list_id ][ $user_id ]['email_address'] = um_user('user_email');
		$_new_subscribers[ $list_id ][ $user_id ]['subscr_status'] = $this->get_subscription_status_by_list_id( $list_id );

		update_option( '_mailchimp_new_subscribers', $_new_subscribers );
		update_option( '_mailchimp_new_unsubscribers', $_new_unsubscribers );

		delete_option( "um_cache_userdata_{$user_id}" );

		if ( UM()->options()->get('mailchimp_real_status') ) {
			$this->mailchimp_subscribe( true );
		}
	}

	/***
	***	@unsubscribe user
	***/
	function unsubscribe( $list_id ) {

		$user_id = $this->user_id;
		um_fetch_user( $user_id );

		if ( !um_user('user_email') ) return;

		$_mylists = get_user_meta( $user_id, '_mylists', true);
		if ( isset($_mylists[$list_id]) ) {
			unset( $_mylists[ $list_id ] );
			update_user_meta( $user_id, '_mylists', $_mylists);
		}

		$_new_subscribers = get_option('_mailchimp_new_subscribers');
		if ( isset( $_new_subscribers[ $list_id ][ $user_id ] ) ) {
			unset($_new_subscribers[$list_id][$user_id]);
		}

		$_new_unsubscribers = get_option('_mailchimp_new_unsubscribers');
		$_new_unsubscribers = is_array( $_new_unsubscribers ) ? $_new_unsubscribers : array();
		$_new_unsubscribers[$list_id][$user_id] = array( 'email_address' => um_user('user_email') );

		update_option( '_mailchimp_new_subscribers', $_new_subscribers );
		update_option( '_mailchimp_new_unsubscribers', $_new_unsubscribers );

		delete_option( "um_cache_userdata_{$user_id}" );

		if ( UM()->options()->get('mailchimp_real_status') ) {
			$this->mailchimp_unsubscribe( true );
		}
	}


	/**
	 * @param $list_id
	 *
	 * @return string
	 */
	function get_subscription_status_by_list_id( $list_id ) {
		global $wpdb;

		$double_optin = $wpdb->get_var("SELECT pm.meta_value 
			FROM {$wpdb->postmeta} pm, {$wpdb->postmeta} pm2
			WHERE pm2.post_id = pm.post_id AND 
				pm.meta_key = '_um_double_optin' AND
				pm2.meta_key = '_um_list' AND
				pm2.meta_value = '{$list_id}'");

		if ( $double_optin == '1' ) {
			$status = 'pending';
		} elseif( $double_optin == '' && UM()->options()->get( 'mailchimp_double_optin' ) ) {
			$status = 'pending';
		} else {
			$status = 'subscribed';
		}

		return $status;
	}


	/***
	***	@Fetch list
	***/
	function fetch_list( $id ) {
		$setup = get_post( $id );
		if ( !isset( $setup->post_title ) ) return false;
		$list['id'] = get_post_meta( $id, '_um_list', true );
		$list['auto_register'] =  get_post_meta( $id, '_um_reg_status', true );
		$list['description'] = get_post_meta( $id, '_um_desc', true );
		$list['register_desc'] = get_post_meta( $id, '_um_desc_reg', true );
		$list['name']  = $setup->post_title;
		$list['status'] = get_post_meta( $id, '_um_status', true );
		$list['merge_vars'] = get_post_meta( $id, '_um_merge', true );
		$list['roles'] = get_post_meta( $id, '_um_roles', true);

		return $list;
	}

	/***
	***	@Check if there are active integrations
	***/
	function has_lists( $admin = false, $user_id = null ) {
		$args = array(
			'post_status'	=> array('publish'),
			'post_type' 	=> 'um_mailchimp',
			'fields'		=> 'ids',
			'posts_per_page' => -1
		);
		$args['meta_query'][] = array('relation' => 'AND');
		$args['meta_query'][] = array(
			'key' => '_um_status',
			'value' => '1',
			'compare' => '='
		);

		if( is_numeric( $user_id ) ){
			$this->user_id = $user_id;
		}

		um_fetch_user( $this->user_id );
		$lists = new \WP_Query( $args );
		if ( $lists->found_posts > 0 ) {
			$array = $lists->get_posts();

			// frontend-use
			if ( !$admin ) {
				foreach( $array as $k => $post_id ) {
					$roles = get_post_meta( $post_id, '_um_roles', true);
					$current_user_roles = um_user( 'roles' );
					if ( ! empty( $roles ) && ( empty( $current_user_roles ) || count( array_intersect( $current_user_roles, $roles ) ) <= 0 ) ) {
						unset( $array[$k] );
					}
				}
			} 

			if ( $array )
				return $array;
		}
		return array();
	}

	/***
	***	@get merge vars for a specific list
	***/
	function get_vars( $list_id ) {

		$apikey = UM()->options()->get('mailchimp_api');
		if ( $apikey ) {

			$api = new \UM_MCAPI( $apikey );

			$merge_vars = $api->call('lists/merge-vars',  array(
				'id' => array( $list_id )
			));

		}

		if ( isset( $merge_vars['data'][0]['merge_vars'] ) )
			return $merge_vars['data'][0]['merge_vars'];
		return array('');
	}

	/***
	***	@subscribe status
	***/
	function is_subscribed( $list_id ) {

		$user_id = $this->user_id;

		$_mylists = get_user_meta( $user_id, '_mylists', true);

		if ( isset( $_mylists[ $list_id ] ) ) {
				return true;
		}

		if ( UM()->options()->get('mailchimp_real_status') ) {

			$email_md5 = md5( um_user('user_email') );
			$lists = $this->call()->get("lists/{$list_id}/members/{$email_md5}");
			if ( !$lists || ( isset( $lists['status'] ) && $lists['status'] == 'unsubscribed' ) || $lists['status'] == 404 ) {
				return false;
			}
			
			return true;
			

		} 

		return false;

	}

	/***
	***	@Get list names
	***/
	function get_lists( $raw = true ) {
		$lists = array();
		if ( $raw ) { // created from MailChimp
			$lists = $this->call()->get( 'lists',  array( "count" => apply_filters('um_mailchimp_lists_limit', 30 ) ) );
		} else { // created from post type 'um_mailchimp'
			$has_lists = $this->has_lists( true );
			if( is_array( $has_lists ) ){
				foreach ( $has_lists as $i => $list_id ){
					$list = $this->fetch_list( $list_id );
					$lists['lists'][] = array(
						'name' => $list['name'],
						'id'   => $list_id,
					);
				}
			}
		}

		$res = array();
		if ( isset( $lists['lists'] ) ) {
			foreach ( $lists['lists'] as $key => $list ) {
				$res[ $list['id'] ] = $list['name'];
			}
		}

		return $res;
	}

	/***
	***	@Get list subscriber count
	***/
	function get_list_member_count( $list_id ) {
		$res = null;
		$apikey = UM()->options()->get('mailchimp_api');
		if ( $apikey ) {
			$api = new \UM_MCAPI( $apikey );
			$lists = $api->call('lists/list');
		}

		if ( !isset( $lists ) ) return __('Please setup MailChimp API','um-mailchimp');
		foreach( $lists['data'] as $key => $list ) {
			if ($list['id'] == $list_id)
				return $list['stats']['member_count'];
		}
		return 0;
	}

	/***
	***	@Retrieve connection
	***/
	function call() {
		if( is_object( $this->mailchimp ) ) return $this->mailchimp;

		$apikey = UM()->options()->get('mailchimp_api');
		if ( !$apikey )
			return new \WP_Error( 'um-mailchimp-empty-api-key',
				sprintf(__('<a href="%s"><strong>Please enter your valid API key</strong></a> in settings.','um-mailchimp'), admin_url('admin.php?page=um_options&tab=extensions&section=mailchimp' ) ) );

		try{
			$result = new \UM_MailChimp_V3( $apikey );
			$this->mailchimp = $result;
		} catch ( \Exception $e ) {
			$result = new \WP_Error( 'um-mailchimp-api-error', $e->getMessage() );
		}

		
		return $result;
	}

	/***
	***	@Retrieve connection
	***/
	function get_account_data() {
		$result = $this->call();
		if( !is_wp_error( $result ) ) {
			$result = $result->get('');
		}

		return $result;
	}

	/***
	***	@Queue count
	***/
	function queue_count( $type ) {
		$count = 0;
		$queue = '';
		if ( $type == 'subscribers' ) {
			$queue = get_option( '_mailchimp_new_subscribers' );
		} elseif ( $type == 'unsubscribers' ) {
			$queue = get_option( '_mailchimp_new_unsubscribers' );
		} else if ( $type == 'update' ) {
			$queue = get_option( '_mailchimp_new_update' );
		/*} else if ( $type == 'not_synced' ) {
			$queue = get_option( '_mailchimp_unable_sync_profiles' );*/
		} else if ( $type == 'not_optedin' ) {
			$queue = get_option( '_mailchimp_not_optedin_profiles' );
		} else if ( $type == 'optedin_not_synced' ) {
			$queue = get_option( '_mailchimp_optedin_not_synced_profiles' );
		} else if ( $type == 'errored_synced_profiles' ) {
			$queue = get_option( '_um_mailchimp_optedin_synced_errored_profiles' );
		}

		if ( $queue && !in_array( $type , array('not_optedin','optedin_not_synced') ) ) {
			foreach( $queue as $list_id => $data ) {
				$count = $count + count($data);
			}
		}else if( $queue ) {
			$count = count( $queue );
		}

		return $count;
	}

	function get_external_list_users( $list_id, $cache = true ) {
		if( $cache ) {
			$mailchimp_members = get_transient('_um_mailchimp_list_members_' . $list_id );
		}

		if( empty( $mailchimp_members ) ) {
			$response          = $this->call()->get( "lists/{$list_id}/members" );
			$mailchimp_members = array();
			if ( ! empty( $response['members'] ) ) {
				foreach ( $response['members'] as $member ) {
					if ( $member['status'] != 'subscribed' ) {
						continue;
					}
					$mailchimp_members[] = $member['email_address'];
				}
			}
		}
		return $mailchimp_members;
	}

	function sync_list_users( $list_id ) {
		//get users list from cache
		if( !( $users = get_transient( '_um_mailchimp_sync_users' ) ) ) {
			//get all users with selected role and status
			$query_users = new \WP_User_Query( array(
				'fields' => array( 'user_email', 'ID' )
			) );
			$users       = $query_users->get_results();
			//set users list cache
			set_transient( '_um_mailchimp_sync_users', $users, 24 * 3600 );
		}

		if( count( $users ) > 0 ) {
			//get subscribers from mailchimp list
			$mailchimp_members = $this->get_external_list_users( $list_id );

			$subscribe = get_transient('_um_mailchimp_subscribe_users');
			foreach ( $users as $key => $user ) {
				$internal_user_lists = isset( $user->internal_lists ) ? $user->internal_lists : get_user_meta( $user->ID, "_mylists", true );
				if( is_array( $internal_user_lists ) && count( $internal_user_lists ) ) {

					//check if user isn't mailchimp subscriber for list with id $list_id but subscribed on current site
					if( !in_array( $user->user_email, $mailchimp_members ) && isset( $internal_user_lists[ $list_id ] ) ) {
						/*$this->mailchimp_subscribe()*/

					//check if user is mailchimp subscriber for list with id $list_id but didn't subscribed on current site
					} else if( in_array( $user->user_email, $mailchimp_members ) && !isset( $internal_user_lists[ $list_id ] ) ) {
						if( !is_array( $subscribe[ $list_id ] ) ) $subscribe[ $list_id ] = array();
						$subscribe[ $list_id ][] = $user;
					}
				} else {
					if( !is_array( $subscribe[ $list_id ] ) ) $subscribe[ $list_id ] = array();
					$subscribe[ $list_id ][] = $user;
				}
			}
			set_transient( '_um_mailchimp_subscribe_users', $subscribe, 24 * 3600 );
		}
	}

	function get_profiles_for_subscription( $action_key, $role = '', $status = '' ) {
		//get users list from cache
		if( !( $users = get_transient( '_um_mailchimp_users_' . $action_key . '_' . $role . '_' . $status ) ) ) {
			//get all users with selected role and status
			$args = array(
				'fields' => array( 'user_email', 'ID' )
			);

			if ( ! empty( $role ) ) {
				$args['role'] = $role;
			}

			if ( ! empty( $status ) ) {
				$args['meta_query'][] = array(
					'key'     => 'account_status',
					'value'   => $status,
					'compare' => '=',
				);
			}

			$query_users = new \WP_User_Query( $args );
			$users       = $query_users->get_results();

			//set users list cache
			set_transient( '_um_mailchimp_users_' . $action_key . '_' . $role . '_' . $status, $users, 24 * 3600 );
		}
		return $users;
	}

	function get_profiles_not_optedin( $role = '', $status = '' ) {
		
		// Not Opted-in
		$args = array(
				'meta_query' => array(
					'relation' => 'AND',
					array(
						'relation' => 'OR',
						array(
							'key'     => '_mylists',
							'compare' => 'NOT EXISTS'
						),
						array(
							'key'     => '_mylists',
							'compare' => '=',
							'value'	  => 'a:0:{}'
						),
						array(
							'key'     => '_mylists',
							'compare' => '=',
							'value'	  => ''
						),
					),

				),
				'fields' => array( 'ID' )
			);


		if( !empty( $role ) ) {
			$args['role'] = $role;
		}

		if( !empty( $status ) ){
			$args['meta_query'][] = array(
				'key' => 'account_status',
				'value' => $status,
				'compare' => '=',
			);
		}
		
		$query_users = new \WP_User_Query( $args );
		$profiles = array();
		foreach( $query_users->get_results() as $user ) {
			$profiles[] = $user->ID;
		}
		update_option( '_mailchimp_not_optedin_profiles', $profiles );
	}

	function get_merge_vars_values( $list_id, $user_id,  $merge_vars = array() ){

			$_merge_vars = array();

			$key_list = array();
			if( is_array( $list_id )  && ! empty( $list_id ) ){
				$key_list = array(
					'key'     => '_um_list',
					'value'   =>  $list_id,
					'compare' => 'IN',
				);
			}else{
				 $key_list = array(
					'key'     => '_um_list',
					'value'   =>  $list_id,
					'compare' => '=',
				);
			}

			$args = array(
				'post_type' => 'um_mailchimp',
				'meta_query' => array(
					'relation' => 'AND',
					array(
									'key' => '_um_status',
									'value' => 1,
									'compare' => '='
					)
				),
				'post_status' => 'publish',
				'posts_per_page' => 1,
				'fields' => 'ids'
			);

			$args['meta_query'][ ] = $key_list;

			$um_list_query  = new \WP_Query( $args );

			if( $um_list_query->found_posts > 0 ){

				um_fetch_user( $user_id );

			   $_merge_vars = get_post_meta( $um_list_query->posts[0], '_um_merge', true );

				if ( $_merge_vars ) {
							foreach( $_merge_vars as $meta => $var ) {
								if ( $var != '0' && um_user( $meta ) ) {
									$merge_vars[ $var ] = um_user( $meta );
								}
							}
				}
			}

			return $merge_vars;

	}

}