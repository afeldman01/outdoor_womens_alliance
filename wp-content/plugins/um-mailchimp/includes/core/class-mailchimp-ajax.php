<?php
namespace um_ext\um_mailchimp\core;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) exit;

class Mailchimp_Ajax {

	function __construct() {
		add_filter( 'um_admin_enqueue_localize_data',  array( &$this, 'localize_data' ), 10, 1 );
	}

	function localize_data( $data ) {

		$data['mailchimp_force_subscribe'] = UM()->get_ajax_route( 'um_ext\um_mailchimp\core\Mailchimp_Ajax', 'ajax_force_subscribe' );
		$data['mailchimp_force_unsubscribe'] = UM()->get_ajax_route( 'um_ext\um_mailchimp\core\Mailchimp_Ajax', 'ajax_force_unsubscribe' );
		$data['mailchimp_force_update'] = UM()->get_ajax_route( 'um_ext\um_mailchimp\core\Mailchimp_Ajax', 'ajax_force_update' );
		$data['mailchimp_scan_now'] = UM()->get_ajax_route( 'um_ext\um_mailchimp\core\Mailchimp_Ajax', 'ajax_scan_now' );
		$data['mailchimp_bulk_subscribe'] = UM()->get_ajax_route( 'um_ext\um_mailchimp\core\Mailchimp_Ajax', 'ajax_bulk_subscribe' );
		$data['mailchimp_sync_now'] = UM()->get_ajax_route( 'um_ext\um_mailchimp\core\Mailchimp_Ajax', 'ajax_sync_now' );

		return $data;

	}

	function ajax_force_subscribe() {

		header('content-type: application/json');

		// force subscribe users
		UM()->Mailchimp_API()->api()->mailchimp_subscribe( true, false );

		// prepare return messages
		$return = array('success' => 1, 'progress' => 0, 'total' => 0, 'message' => rand());
		$total  = UM()->Mailchimp_API()->api()->queue_count( 'subscribers' );


		// update return messages
		$return['total']   = $total;
		$return['message'] = sprintf("%d unprocessed users left.", $total);

		if(!$total) {
			$return['progress'] = 100;
		}

		// display the results
		echo json_encode($return);
		die();
	}


	function ajax_force_unsubscribe() {
		header('content-type: application/json');

		// force unsubscribe users
		UM()->Mailchimp_API()->api()->mailchimp_unsubscribe( true, false );

		// prepare return messages
		$return = array('success' => 1, 'progress' => 0, 'total' => 0, 'message' => rand());
		$total  = UM()->Mailchimp_API()->api()->queue_count( 'unsubscribers' );


		// update return messages
		$return['total']   = $total;
		$return['message'] = sprintf("%d unprocessed users left.", $total);

		if(!$total) {
			$return['progress'] = 100;
		}

		// display the results
		echo json_encode($return);
		die();
	}


	function ajax_force_update() {
		header('content-type: application/json');

		// force update users
		UM()->Mailchimp_API()->api()->mailchimp_update( true, false );

		// prepare return messages
		$return = array('success' => 1, 'progress' => 0, 'total' => 0, 'message' => rand());
		$total  = UM()->Mailchimp_API()->api()->queue_count( 'update' );


		// update return messages
		$return['total']   = $total;
		$return['message'] = sprintf("%d unprocessed users left.", $total);

		if(!$total) {
			$return['progress'] = 100;
		}

		// display the results
		echo json_encode($return);
		die();
	}


	function ajax_sync_now() {
		if( !empty( $_POST['list'] ) ) {
			$list_id = $_POST['list'];
			if( !( $list = UM()->Mailchimp_API()->api()->fetch_list( $list_id ) ) ) {
				wp_send_json_error( __('Wrong list') );
			}
		} else {
			wp_send_json_error( __('Empty list ID') );
		}

		$action_key = empty( $_POST['key'] ) ? uniqid() : $_POST['key'];
		$users = UM()->Mailchimp_API()->api()->get_profiles_for_subscription( $action_key );

		if( count( $users ) ) {
			if ( function_exists( 'set_time_limit' ) &&
				 false === strpos( ini_get( 'disable_functions' ), 'set_time_limit' ) &&
				 ! ini_get( 'safe_mode' )
			) { // phpcs:ignore PHPCompatibility.PHP.DeprecatedIniDirectives.safe_modeDeprecatedRemoved
				@set_time_limit( 0 ); // @codingStandardsIgnoreLine
			}

			$Batch             = UM()->Mailchimp_API()->api()->call()->new_batch();
			$mailchimp_members = UM()->Mailchimp_API()->api()->get_external_list_users( $list['id'] );

			$internal_lists = array_keys( UM()->Mailchimp_API()->api()->get_lists( false ) );
			foreach ( $users as $user ) {
				um_fetch_user( $user->ID );
				$user_internal_lists = um_user('_mylists');
				$user_internal_lists = is_array( $user_internal_lists ) ? $user_internal_lists : array();

				$data = array('FNAME'=> um_user('first_name'), 'LNAME'=> um_user('last_name') );
				$data = UM()->Mailchimp_API()->api()->get_merge_vars_values( $list['id'], $user->ID, $data );

				if( in_array( $user->user_email, $mailchimp_members ) && empty( $user_internal_lists[ $list['id'] ] ) ) {
					//user only in mailchimp list
					$user_internal_lists[ $list['id'] ] = 1;
					update_user_meta( $user->ID,'_mylists', $user_internal_lists );
				} else if( !in_array( $user->user_email, $mailchimp_members ) && !empty( $user_internal_lists[ $list['id'] ] ) ) {
					//user only in internal list
					$Batch->post("op_uid_{$user->ID}_list_{$list_id}_{$action_key}", "lists/{$list['id']}/members", array(
						'email_address' => $user->user_email,
						'status'        => 'subscribed',
						'merge_fields'  => $data
					) );
				} else if( in_array( $user->user_email, $mailchimp_members ) && !empty( $user_internal_lists[ $list['id'] ] ) ) {
					//user in both lists, need only update data in mailchimp list
					$Batch->put("op_uid_{$user->ID}_list_{$list_id}_{$action_key}", "lists/{$list['id']}/members", array(
						'email_address' => $user->user_email,
						'status'        => 'subscribed',
						'merge_fields'  => $data
					) );
				}
			}
			$batch_id = $Batch->execute();
			$index = !empty( $_POST['index'] ) ? (int)$_POST['index'] + 1 : 1;
			$total = !empty( $_POST['total'] ) ? (int)$_POST['total'] : 1;
			if( $index == $total ) {
				$message = __('Completed', 'um-mailchimp');
			} else {
				$message = sprintf( __('Processed... %s', 'um-mailchimp'), ($index/$total*100) . '%' );
			}
			wp_send_json_success( array(
				'batch_id' => $batch_id,
				'message' => $message,
				'key' => $action_key
			) );
		} else {
			wp_send_json_error( __( 'You don\'t have any users', 'um-mailchimp' ) );
		}
	}


	function ajax_scan_now() {
		$role = isset( $_POST['role'] ) ? $_POST['role'] : '';
		$status = isset( $_POST['status'] ) ? $_POST['status'] : '';
		$action_key = isset( $_POST['key'] ) ? $_POST['key'] : uniqid();

		$users = UM()->Mailchimp_API()->api()->get_profiles_for_subscription( $action_key, $role, $status );

		// display the results
		wp_send_json_success( array(
			'key' => $action_key,
			'total' => count( $users ),
			'scan_total_message' => sprintf( _n( '%d user was selected', '%d users were selected', count( $users ), 'um-mailchimp' ), count( $users ) )
		) );
	}


	function ajax_bulk_subscribe() {
		if( !empty( $_POST['list'] ) ) {
			$list_id = $_POST['list'];
			if( !( $list = UM()->Mailchimp_API()->api()->fetch_list( $list_id ) ) ) {
				wp_send_json_error( __('Wrong list') );
			}
		} else {
			wp_send_json_error( __('Empty list ID') );
		}

		if( !empty( $_POST['key'] ) ) {
			$action_key = $_POST['key'];
		} else {
			wp_send_json_error( __('Empty key') );
		}
		$role = isset( $_POST['role'] ) ? $_POST['role'] : '';
		$status = isset( $_POST['status'] ) ? $_POST['status'] : '';

		$users = UM()->Mailchimp_API()->api()->get_profiles_for_subscription( $action_key, $role, $status );

		if( count( $users ) ) {
			if( function_exists( 'set_time_limit' ) &&
				false === strpos( ini_get( 'disable_functions' ), 'set_time_limit' ) &&
				!ini_get( 'safe_mode' ) ) { // phpcs:ignore PHPCompatibility.PHP.DeprecatedIniDirectives.safe_modeDeprecatedRemoved
					@set_time_limit( 0 ); // @codingStandardsIgnoreLine
			}

			$Batch = UM()->Mailchimp_API()->api()->call()->new_batch();
			$mailchimp_members = UM()->Mailchimp_API()->api()->get_external_list_users( $list['id'] );

			foreach( $users as $user ) {
				um_fetch_user( $user->ID );
				$data = array('FNAME'=> um_user('first_name'), 'LNAME'=> um_user('last_name') );
				$data = UM()->Mailchimp_API()->api()->get_merge_vars_values( $list['id'], $user->ID, $data );

				if( !in_array( $user->user_email, $mailchimp_members ) ) {
					$Batch->post("op_uid_{$user->ID}_list_{$list_id}_{$action_key}", "lists/{$list['id']}/members", array(
						'email_address' => $user->user_email,
						'status'        => 'subscribed',
						'merge_fields'  => $data
					) );
				} else {
					$Batch->put("op_uid_{$user->ID}_list_{$list_id}_{$action_key}", "lists/{$list['id']}/members", array(
						'email_address' => $user->user_email,
						'status'        => 'subscribed',
						'merge_fields'  => $data
					) );
				}

				$user_lists = get_user_meta( $user->ID, '_mylists', true );
				if( !isset( $user_lists[ $list['id'] ] ) ) {
					$user_lists[ $list['id'] ] = 1;
					update_user_meta( $user->ID, '_mylists', $user_lists );
				}
			}
			$batch_id = $Batch->execute();
			$index = !empty( $_POST['index'] ) ? (int)$_POST['index'] + 1 : 1;
			$total = !empty( $_POST['total'] ) ? (int)$_POST['total'] : 1;
			if( $index == $total ) {
				$message = __('Completed', 'um-mailchimp');
			} else {
				$message = sprintf( __('Processed... %s', 'um-mailchimp'), ($index/$total*100) . '%' );
			}
			wp_send_json_success( array( 'batch_id' => $batch_id, 'message' => $message ) );
		} else {
			wp_send_json_error( __('Empty users list') );
		}
	}

}