<?php

// BEGIN iThemes Security - Do not modify or remove this line
// iThemes Security Config Details: 2
define('WP_CACHE', true);
define( 'DISALLOW_FILE_EDIT', true ); // Disable File Editor - Security > Settings > WordPress Tweaks > File Editor
define( 'FORCE_SSL_ADMIN', true ); // Force SSL for Dashboard - Security > Settings > Secure Socket Layers (SSL) > SSL for Dashboard
// END iThemes Security - Do not modify or remove this line

/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress

*/
/*
define( 'WPCACHEHOME', '/var/www/owa/wp-content/plugins/wp-super-cache/' );
$redis_server = array(
    'host'     => '127.0.0.1',
    'port'     => 6379,
    'database' => 0, // Optionally use a specific numeric Redis database. Default is 0.
);
*/
//define('WP_HOME','http://owa.heartski.com');
//define('WP_SITEURL','http://owa.heartski.com');

//define('WP_TEMP_DIR', '/var/www/temp/');

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME','outdoorw_wp1');

/** MySQL database username */
define('DB_USER','owaaccess');

/** MySQL database password */
define('DB_PASSWORD','3L8WNsVVRapMMAAN');

/** MySQL hostname */
define('DB_HOST', '35.227.85.166');  //onyx-elevator-192018:us-east1:production  35.227.85.166

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

//define('DISALLOW_FILE_EDIT', false);

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '26ZPvuRLbjv5SN4egUOj2Mxy2gTSKQ7n8v3Gi0VT65kBXh6HGcfebxexuY7tHAFn');
define('SECURE_AUTH_KEY',  '9iQJFCCNqBKv98P2qB4hUPt7mqroO6DTyO54RfeWLMu5HUWHDrXRTET5yCOrUZJ8');
define('LOGGED_IN_KEY',    'ftV94JL3vs7Gfq8cc6dJeZwXhIvEBH099aQV2mrSU4m8u0qJnpZ6o1erLQ1k6kaN');
define('NONCE_KEY',        'XKsMQ1vqY8aR7agGh5vfBz35Oc3pBqLDpF1LAC0HlgEMPG2M7EvcnmGtrgs1k3Xp');
define('AUTH_SALT',        'JJdg1c7IARjGq5vgBzViKVsTMc2WBtpqK6U9SiVDn1bj5fpTHoLswPgfjvAduUaf');
define('SECURE_AUTH_SALT', 'qrzOoBbwwohHJoM7MVesAPqT1OMzmQrEPEPUqicWnmgzcjybjalVOjZ0GqSGQXJo');
define('LOGGED_IN_SALT',   'yKbUraaDZvr3EvA6mIZPHOGJvZLt6JsanDFvmCiKIg954CfPUzce3gk16Cn0HEIy');
define('NONCE_SALT',       'wvDghb8FJvdXZszCUYqAruXm3wZHhBqktTOShgMohfhHhXleDFzw44PXevXVR5b4');

/**
 * Other customizations.
 */
define('FS_METHOD','direct');define('FS_CHMOD_DIR',0755);define('FS_CHMOD_FILE',0644);
define('WP_TEMP_DIR',dirname(__FILE__).'/wp-content/uploads');
define( 'WP_MEMORY_LIMIT', '256M' );

/**
 * Turn off automatic updates since these are managed upstream.
 */
define('AUTOMATIC_UPDATER_DISABLED', true);


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

# Disables all core updates. Added by SiteGround Autoupdate:
define( 'WP_AUTO_UPDATE_CORE', false );
